import React, { useState } from 'react';

function HatsForm() {
  const [formData, setFormData] = useState({
    fabric: '',
    style: '',
    color: '',
    image: '',
  });

  const handleSubmit = async (event) => {
    event.preventDefault();

    const hatUrl = 'http://localhost:8090/api/hats/';

    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(hatUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        fabric: '',
        style: '',
        color: '',
        image: '',
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input
                value={formData.fabric}
                onChange={handleFormChange}
                placeholder="Fabric"
                required
                type="text"
                name="fabric"
                id="fabric"
                className="form-control"
              />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.style}
                onChange={handleFormChange}
                placeholder="Style"
                required
                type="text"
                name="style"
                id="style"
                className="form-control"
              />
              <label htmlFor="style">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.color}
                onChange={handleFormChange}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.image}
                onChange={handleFormChange}
                placeholder="Image URL"
                required
                type="text"
                name="image"
                id="image"
                className="form-control"
              />
              <label htmlFor="image">Image URL</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatsForm;
