import React, { useEffect, useState } from 'react';

function ShoesForm() {
    const [manufacturer, setManufacturer] = useState('')
    const [name, setName] = useState('')
    const [color, setColor] = useState('')
    const [state, setState] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.name = name;
        data.color = color;
        data.state = state

        console.log(data);

        const locationUrl = 'http://localhost:8000/api/createshoe';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': "application/json"
            }
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setManufacturer('');
            setName('');
            setColor('');
        }

    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setStates(data.states)
        }
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleNameChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);
    }

    useEffect(()  => {
        fetchData();
    }, [])

    return (
        <>
        <div class="row">
            <div class="offset-3 col-6">
                <div class="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div class="form-floating mb-3">
                        <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="name" id="name" className="form-control" />
                            <label for="name">Name</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required type="number" name="room_count" id="room_count" className="form-control" />
                            <label for="room_count">Room count</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" required type="text" name="city" id="city" class="form-control"></input>
                            <label for="city">City</label>
                        <div class="mb-3">
                            <select onChange={handleStateChange} required name="state" id="state" class="form-select">
                                <option selected value="">Choose a state</option>
                            {states.map(state => {
                                return (
                                    <option key={state.abbreviation} value={state.abbreviation}>
                                        {state.name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        </div>
                        <button class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        </>
    )
}

export default ShoesForm;
