import { useEffect, useState } from 'react';

function HatsList() {
  const [hats, setHats] = useState([]);

  const getData = async () => {
      const response = await fetch('http://localhost:8090/api/hats/');

      if (response.ok) {
        const data = await response.json();
        setHats(data.hats)
      }
    }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Fabric</th>
          <th>Style</th>
          <th>Color</th>
          <th>Image</th>
        </tr>
      </thead>
      <tbody>
        {hats.map(hat => {
          return (
            <tr key={ hat.id }>
              <td>{ hat.fabric }</td>
              <td>{ hat.style }</td>
              <td>{ hat.color }</td>
              <td>{ hat.image }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatsList
