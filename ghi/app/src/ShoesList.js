import { useEffect, useState } from 'react';

function ShoesList() {
    const [shoes, setShoes] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api.shoes/');

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Name</th>
                    <th>Color</th>
                    <th>Image</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                    <tr key={shoe.href}>
                        <td>{ shoe.name }</td>
                        <td>{ shoe.image }</td>
                    </tr>
              );
            })}
          </tbody>
        </table>
      );
}

export default ShoesList;
