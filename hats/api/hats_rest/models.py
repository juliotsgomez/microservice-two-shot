from django.db import models

# Create your models here.
class LocationVO(models.Model):
    closet_name = models.CharField(max_length=150, default="SOME STRING")
    import_href = models.CharField(max_length=200, unique=True, default="SOME STRING")


class Hat(models.Model):
    fabric = models.CharField(max_length=150)
    style = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    image = models.URLField()
    location = models.ForeignKey(
                    LocationVO,
                    related_name="bins",
                    on_delete=models.CASCADE,
                )
