from django.urls import path
from . import views


urlpatterns = [
    path("api/hats/", views.api_list_hats, name="api_list_hats"),
]
