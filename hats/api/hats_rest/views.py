from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
import json
from common.json import ModelEncoder


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
    ]


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "image",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    location = None

    if request.method == "GET":
        location_href = request.GET.get("location")
        if location_href:
            try:
                location = LocationVO.objects.get(import_href=location_href)
            except LocationVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid location id"},
                    status=400
                )

        hats = Hat.objects.filter(location=location)
        return JsonResponse(
            {"hats": list(hats)},
            encoder=HatEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False
        )
