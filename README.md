# Wardrobify

Team:

* Person 1 - Which microservice?
* Person 2 - Which microservice?
Julio- Shoes
Kai - Hats

## Design
REACT
## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

We used polling to use a bin virtual object to interact with the wardrobes bin.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Julio and I used polling to access the wardrobe's location files through my own location virtual object.
