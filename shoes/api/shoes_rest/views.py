from django.shortcuts import render
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "name", "color", "image", "bin"]
    encoders = {
        "bin": BinVOEncoder(),
    }

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    bin=None
    if request.method == "GET":
        shoes = Shoe.objects.filter(bin)
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False
        )
