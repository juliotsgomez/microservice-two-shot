from django.db import models


class BinVO(models.Model):
    closet_name = models.CharField(max_length=150, default='SOME STRING')
    import_href = models.CharField(max_length=200, unique=True, default='SOME STRING')


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=150)
    name = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    image = models.URLField()
    bin = models.ForeignKey(
        BinVO,
        related_name="bins",
        on_delete=models.CASCADE,
    )
